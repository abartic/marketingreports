-- phpMyAdmin SQL Dump
-- version 4.2.5
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jul 06, 2014 at 01:33 PM
-- Server version: 5.6.19
-- PHP Version: 5.3.10-1ubuntu3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `mrdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `campaign`
--

CREATE TABLE `campaign` (
`campaign_id` int(11) NOT NULL,
  `campaign_descr` varchar(250) NOT NULL,
  `load_date` date NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `file_name` varchar(255) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `campaign`
--

INSERT INTO `campaign` (`campaign_id`, `campaign_descr`, `load_date`, `start_date`, `end_date`, `file_name`) VALUES
(1, 'alex c', '2014-01-01', '2014-01-01', '2014-01-01', ''),
(2, 'gabi', '2003-01-01', '2003-01-01', '2003-01-01', ''),
(3, 'gigi_camp', '2002-12-31', '2002-12-31', '2002-12-31', ''),
(4, 'gigi 2', '2003-01-01', '2003-01-01', '2003-01-01', ''),
(5, 'fgfgf', '2003-01-01', '2003-01-01', '2003-01-01', ''),
(7, 'gogu', '2003-01-01', '2003-01-01', '2003-01-01', '');

-- --------------------------------------------------------

--
-- Table structure for table `campaign_data`
--

CREATE TABLE `campaign_data` (
`campaign_data_id` int(11) NOT NULL,
  `campaign_id` int(11) NOT NULL,
  `no_week` int(11) NOT NULL,
  `week_descr` varchar(255) NOT NULL,
  `medium` varchar(255) NOT NULL,
  `ad_type` varchar(255) NOT NULL,
  `country` varchar(100) NOT NULL,
  `impressions` int(11) NOT NULL,
  `clicks` int(11) NOT NULL,
  `cost` decimal(10,0) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `campaign_data`
--

INSERT INTO `campaign_data` (`campaign_data_id`, `campaign_id`, `no_week`, `week_descr`, `medium`, `ad_type`, `country`, `impressions`, `clicks`, `cost`) VALUES
(1, 1, 0, '', '', '', '', 0, 0, 0),
(2, 1, 0, '', '', '', '', 0, 0, 0),
(3, 1, 0, '', '', '', '', 0, 0, 0),
(4, 2, 0, '', '', '', '', 0, 0, 0),
(5, 2, 0, '', '', '', '', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
`user_id` int(11) NOT NULL,
  `user_name` varchar(45) NOT NULL,
  `user_pwd` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `campaign`
--
ALTER TABLE `campaign`
 ADD PRIMARY KEY (`campaign_id`);

--
-- Indexes for table `campaign_data`
--
ALTER TABLE `campaign_data`
 ADD PRIMARY KEY (`campaign_data_id`), ADD KEY `campaign_id` (`campaign_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
 ADD PRIMARY KEY (`user_id`), ADD UNIQUE KEY `user_name` (`user_name`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `campaign`
--
ALTER TABLE `campaign`
MODIFY `campaign_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `campaign_data`
--
ALTER TABLE `campaign_data`
MODIFY `campaign_data_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `campaign_data`
--
ALTER TABLE `campaign_data`
ADD CONSTRAINT `FK_campaing` FOREIGN KEY (`campaign_id`) REFERENCES `campaign` (`campaign_id`) ON DELETE CASCADE ON UPDATE CASCADE;
