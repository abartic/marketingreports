package controller;

import model.Campaign;
import controller.util.JsfUtil;
import controller.util.PaginationHelper;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import javax.sql.DataSource;
import model.CampaignData;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

@Named("campaignController")
@SessionScoped
public class CampaignController implements Serializable {

    private Campaign current;
    private DataModel items = null;
    @EJB
    private controller.CampaignFacade ejbFacade;
    private PaginationHelper pagination;
    private int selectedItemIndex;

    @Inject
    ServletContext svrcontext;

    @EJB
    private controller.CampaignDataFacade ejbCampaignDataFacade;

    public CampaignController() {
    }

    public Campaign getSelected() {
        if (current == null) {
            current = new Campaign();
            selectedItemIndex = -1;
        }
        return current;
    }

    private CampaignFacade getFacade() {
        return ejbFacade;
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {

                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize()}));
                }
            };
        }
        return pagination;
    }

    public String prepareList() {
        recreateModel();
        return "List";
    }

    public String prepareView() {
        current = (Campaign) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "View";
    }

    public String prepareCreate() {
        current = new Campaign();
        selectedItemIndex = -1;
        return "Create";
    }

    public String create() {
        try {
            getFacade().create(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("web.Bundle").getString("CampaignCreated"));
            return prepareCreate();
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("web.Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String prepareEdit() {
        current = (Campaign) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "Edit";
    }

    public String update() {
        try {
            Boolean hasFile = false;
            if (current.getFile().getSize() > 0) {
                String fileName = getFilename(current.getFile());
                current.setFileName(fileName);
                hasFile = true;
            }

            getFacade().edit(current);

            if (hasFile == true) {
                try {
                    upload();
                } catch (IOException e) {
                    JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("web.Bundle").getString("PersistenceErrorOccured"));
                    return null;
                }
            }

            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("web.Bundle").getString("CampaignUpdated"));
            return "View";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("web.Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String destroy() {
        current = (Campaign) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "List";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("web.Bundle").getString("CampaignDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("web.Bundle").getString("PersistenceErrorOccured"));
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    public Campaign getCampaign(java.lang.Integer id) {
        return ejbFacade.find(id);
    }

    public void upload() throws IOException {

        try (InputStream inputStream = current.getFile().getInputStream()) {
            String finalPath = svrcontext.getRealPath("/Temp/" + current.getFileName());
            try (FileOutputStream outputStream = new FileOutputStream(finalPath)) {
                byte[] buffer = new byte[4096];
                int bytesRead;
                while (true) {
                    bytesRead = inputStream.read(buffer);
                    if (bytesRead > 0) {
                        outputStream.write(buffer, 0, bytesRead);
                    } else {
                        break;
                    }
                }
            }
        }
    }

    public void generateReport() throws SQLException, JRException, IOException, NamingException {
        current = (Campaign) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        
        InitialContext ctx = new InitialContext();
        DataSource ds = (DataSource) ctx.lookup("MarketingReports"); //ds.getConnection(); //
        Connection con =  DriverManager.getConnection("jdbc:mysql://db4free.org:3306/mrdb", "abartic", "alex1977");
        
        String reportPath = svrcontext.getRealPath("/CampaingReport.jrxml");
        JasperReport jasperReport = JasperCompileManager.compileReport(reportPath);
        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, new HashMap(), con);

        String reportPdfPath = svrcontext.getRealPath("/Temp/temp_report.pdf");
        JasperExportManager.exportReportToPdfFile(jasperPrint, reportPdfPath);

        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        HttpServletResponse response = (HttpServletResponse) context.getResponse();  ///HttpServletResponse is not being found...  
        response.sendRedirect("../Temp/temp_report.pdf");

    }

    public void importExcelFile() throws FileNotFoundException, IOException {
        current = (Campaign) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();

        String filePath = svrcontext.getRealPath("/Temp/" + current.getFileName());
        FileInputStream input = new FileInputStream(filePath);

        try {
            if (filePath.endsWith(".xlsx") == true) {
                XSSFWorkbook wb = new XSSFWorkbook(input);
                XSSFSheet sheet = wb.getSheetAt(0);

                XSSFRow row;
                for (int i = 1; i <= sheet.getLastRowNum(); i++) {
                    CampaignData camp_data = new CampaignData();
                    row = sheet.getRow(i);
                    Double dtemp = row.getCell(0).getNumericCellValue();

                    camp_data.setNoWeek(dtemp.intValue());
                    camp_data.setWeekDescr(row.getCell(1).getStringCellValue());
                    camp_data.setMedium(row.getCell(2).getStringCellValue());
                    camp_data.setAdType(row.getCell(3).getStringCellValue());
                    camp_data.setCountry(row.getCell(4).getStringCellValue());

                    dtemp = row.getCell(6).getNumericCellValue();
                    camp_data.setImpressions(dtemp.intValue());

                    dtemp = row.getCell(7).getNumericCellValue();
                    camp_data.setClicks(dtemp.intValue());

                    dtemp = row.getCell(8).getNumericCellValue();
                    camp_data.setCost(dtemp);
                    camp_data.setCampaignId(current);

                    ejbCampaignDataFacade.edit(camp_data);
                }
            } else {

                POIFSFileSystem fs = new POIFSFileSystem(input);
                HSSFWorkbook wb = new HSSFWorkbook(fs);
                HSSFSheet sheet = wb.getSheetAt(0);
                Row row;
                for (int i = 1; i <= sheet.getLastRowNum(); i++) {
                    CampaignData camp_data = new CampaignData();
                    row = sheet.getRow(i);
                    Double dtemp = row.getCell(0).getNumericCellValue();

                    camp_data.setNoWeek(dtemp.intValue());
                    camp_data.setWeekDescr(row.getCell(1).getStringCellValue());
                    camp_data.setMedium(row.getCell(2).getStringCellValue());
                    camp_data.setAdType(row.getCell(3).getStringCellValue());
                    camp_data.setCountry(row.getCell(4).getStringCellValue());

                    dtemp = row.getCell(6).getNumericCellValue();
                    camp_data.setImpressions(dtemp.intValue());

                    dtemp = row.getCell(7).getNumericCellValue();
                    camp_data.setClicks(dtemp.intValue());

                    dtemp = row.getCell(8).getNumericCellValue();
                    camp_data.setCost(dtemp);
                    camp_data.setCampaignId(current);

                    ejbCampaignDataFacade.edit(camp_data);
                }
            }

            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("web.Bundle").getString("CampaignDataUpdated"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("web.Bundle").getString("PersistenceErrorOccured"));
        }
    }

    private String getFilename(Part part) {
        for (String cd : part.getHeader("content-disposition").split(";")) {
            if (cd.trim().startsWith("filename")) {
                String filename = cd.substring(cd.indexOf('=') + 1).trim().replace("\"", "");
                return filename.substring(filename.lastIndexOf('/') + 1).substring(filename.lastIndexOf('\\') + 1); // MSIE fix.  
            }
        }
        return null;
    }

    @FacesConverter(forClass = Campaign.class)
    public static class CampaignControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            CampaignController controller = (CampaignController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "campaignController");
            return controller.getCampaign(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Campaign) {
                Campaign o = (Campaign) object;
                return getStringKey(o.getCampaignId());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + Campaign.class.getName());
            }
        }

    }

}
